package controller;

import java.util.Scanner;

import model.data_structures.Cola;
import model.data_structures.Node;
import model.data_structures.Viaje;
import model.logic.MVCModelo;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}
		
	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
			case 1:
				modelo.cargar();
				System.out.println("El total de viajes es de: " + modelo.darTamano()  +"   " + modelo.darTamano2());
				Viaje primer = modelo.darCola().darPrimer().darDato();
				Viaje ultimo = modelo.darCola().darUltimo().darDato();
				System.out.println("Primer Viaje->   Zona Origen: " + primer.darSourceid() + "    Zona Destino: " + primer.darDtid() + "    Hora: " + primer.darHod() + " Tiempo Promedio: " + primer.darMean_travel_time());
				System.out.println("Ultimo Viaje->   Zona Origen: " + ultimo.darSourceid() + " Zona Destino: " + ultimo.darDtid() + " Hora: " + ultimo.darHod() + " Tiempo Promedio: " + ultimo.darMean_travel_time());
				System.out.println();
				break;

			case 2:
				System.out.println("Ingrese la hora deseada: ");
				dato = lector.next();
				int hora = Integer.parseInt(dato);
				Cola<Viaje> cola = modelo.darCluster(hora);
				Node<Viaje> actual = cola.darPrimer();
				System.out.println("Mayor Cluster:"); 
				if(cola.darTama�o()==0)
				{
					System.out.println("No hay clusters con la hora especificada.");
				}
				while(actual!=null)
				{
					System.out.println("Hora: " + actual.darDato().darHod() + " Zona Origen: " + actual.darDato().darSourceid() + " Zona Destino: " + actual.darDato().darDtid() + " Tiempo Promedio: " + actual.darDato().darMean_travel_time());
					actual = actual.darSiguiente();
				}
				System.out.println();
				
				break;

				case 3:
					System.out.println("--------- \nIngresar n�mero de viajes: ");
					dato = lector.next();
					int param=Integer.parseInt(dato);
					System.out.println("--------- \nIngresar hora: ");
					dato=lector.next();
					int param2= Integer.parseInt(dato);
					Cola<Viaje> colaa= modelo.nviajeshoradada(param, param2);
					int cont=0;
					Node<Viaje>ant= colaa.darPrimer();
					if(ant!=null)
					{
					while(cont<=colaa.darTama�o() &&ant!=null)
					{
						System.out.println("Hora: " + ant.darDato().darHod()+" Origen: "  +ant.darDato().darSourceid()+" Destino: " + ant.darDato().darDtid()+" Tiempo promedio: " + ant.darDato().darMean_travel_time());
						cont++;
						ant=ant.darSiguiente();
					}
					}
					
					break;
					
				case 4: 
					System.out.println("--------- \n Hasta pronto !! \n---------"); 
					lector.close();
					fin = true;
					break;	

				default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
		}
		
	}	
}
