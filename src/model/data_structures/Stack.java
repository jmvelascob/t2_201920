package model.data_structures;

import java.util.Iterator;

public class Stack <T> implements IStack<T>{



	private Node<T> primero;

	private int tama�o;

	public Stack()
	{
		primero= null;
		tama�o=0;
	}

	public void push(T item) {
		
		Node<T> temp= primero;
		primero=new Node<T>(item);
		primero.cambiarDato(item);
		primero.cambiarSiguiente(temp);
		tama�o++;
	}

	@Override
	public T pop() {
		// TODO Auto-generated method stub
		if (!esVacio())
		{
			T temp= primero.darDato();
			primero= primero.darSiguiente();
			tama�o--; 
			return temp;
		}
		else
		{
			return null;
		}
	}


	public boolean esVacio() {
		if (primero==null)
			return true;
		else
			return false;
	}


	public int darTama�o() {
		// TODO Auto-generated method stub
		return tama�o;
	}

	public Node<T> top()
	{
		return primero;
	}

	public Iterator<T> iterator()
	{
		return new ListIterator(primero);
	}

	
	
	
	private class ListIterator implements Iterator<T> {

		private Node<T>actual;

		public ListIterator(Node<T>primeroo)
		{
			actual=primeroo;
		}

		public boolean hasNext() {
			return actual != null;
		}

		@Override
		public T next(){
			T dat= actual.darDato();
			actual=actual.darSiguiente();
			return dat;
		}
	}

}
