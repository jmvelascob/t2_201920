package model.data_structures;

public interface IStack <T> {

	
	void push (T item);
	
	T pop();
	
	boolean esVacio();
	
	int darTama�o();
	
	Node<T> top();
	
}
