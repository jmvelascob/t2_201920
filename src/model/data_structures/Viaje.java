
	package model.data_structures;

	import java.io.Serializable;

	public class Viaje implements Serializable, Comparable<Viaje>
	{
		private int sourceid;
		
		private int hod;

		private int dtid;
		
		private double mean_travel_time;
		
		private double standard_deviation_travel_time;
		
		private double geometric_mean_travel_time;
		
		private double geometric_standard_deviation_travel_time;
		
		public Viaje(int Psourceid, int Pdtid,int hodd, double Pmean_travel_time, double Pstandard_deviation_travel_time, double Pgeometric_mean_travel_time, double Pgeometric_standard_deviation_travel_time)
		{
			hod= hodd;
			sourceid = Psourceid;
			dtid = Pdtid;
			mean_travel_time = Pmean_travel_time;
			standard_deviation_travel_time = Pstandard_deviation_travel_time;
			geometric_mean_travel_time = Pgeometric_mean_travel_time;
			geometric_standard_deviation_travel_time = Pgeometric_standard_deviation_travel_time;
		}
		
		public int darSourceid()
		{
			return sourceid;
		}
		
		public int darDtid()
		{
			return dtid;
		}
		

		public double darMean_travel_time()
		{
			return mean_travel_time;
		}
		
		public double darStandard_deviation_travel_time()
		{
			return standard_deviation_travel_time;
		}
		
		public double darGeometric_mean_travel_time()
		{
			return geometric_mean_travel_time;
		}
		
		public double darGeometric_standard_deviation_travel_time()
		{
			return geometric_standard_deviation_travel_time;
		}
		public int darHod()
		{
			return hod;
		}
		@Override
		public int compareTo(Viaje arg0) 
		{
			
			return 0;
		}	
	}


