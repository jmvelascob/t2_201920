package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.opencsv.CSVReader;

import model.data_structures.Cola;
import model.data_structures.ICola;
import model.data_structures.IStack;
import model.data_structures.Node;
import model.data_structures.Stack;
import model.data_structures.Viaje;

public class MVCModelo {
	/**
	 * Atributos del modelo del mundo
	 */
	private Cola<Viaje> cola;
	private Stack<Viaje> stack;
	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		cola = new Cola<Viaje>();
		stack = new Stack<Viaje>();
	}
	
	
	/**
	 * Servicio de consulta de numero de elementos presentes en el modelo 
	 * @return numero de elementos presentes en el modelo
	 */
	public int darTamano()
	{
		return cola.darTama�o();
	}
	public int darTamano2()
	{
		return stack.darTama�o();
	}
	
	public ICola<Viaje> darCola()
	{
		return cola;
	}
	
	public void cargar()
	{
		CSVReader reader = null;
		try {
			
			reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-1-All-HourlyAggregate.csv"));
			try{
			String[] x =reader.readNext();
			}
			catch(Exception e)
			{
				System.out.println("Problemas leyendo la primera linea");
			}
			for(String[] nextLine : reader) 
			{
			    Viaje dato = new Viaje( Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6]));
			    cola.enqueue(dato);
			    stack.push(dato);
		    }
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		finally
		{
			if (reader != null) 
			{
				try 
				{
					reader.close();
				} catch (IOException e) 
				{
					e.printStackTrace();
				}
			}

		}
	}
	
	//Retorna una lista con el mayor cluster que empieza con una hora especificada por el usuario.
	
		public Cola<Viaje> darCluster( int hora)
		{
			Cola<Viaje> nueva = new Cola<Viaje>();
			Node<Viaje> actual = cola.darPrimer();
			boolean inicio = true;
			
			while(actual != null)
			{
				if(actual.darDato().darHod() == hora)
				{
					Node<Viaje> actualito = actual;
					Cola<Viaje> listaProv = new Cola<Viaje>();
					listaProv.enqueue(actualito.darDato());
					boolean termino = false;
					
					while(actualito.darDato().darHod() <= actualito.darSiguiente().darDato().darHod() && !termino)
					{
						listaProv.enqueue(actualito.darSiguiente().darDato());
						if(actualito.darSiguiente()!=null)
						{
							actualito = actualito.darSiguiente();
						}
						else
						{
							termino = true;
						}
						
					}
					if(listaProv.darTama�o()>nueva.darTama�o())
					{
						nueva = listaProv;
					}
					actual = actualito;
				}
				actual = actual.darSiguiente();
			}
			return nueva;
		}

		//Retorna un n�mero de viajes en una hora dada 
	public Cola<Viaje> nviajeshoradada(int n, int hora)
	{
		int cont=0;
		Cola<Viaje> ccola= new Cola<Viaje>();
		Node<Viaje> ant= stack.top();

		while(cont<n && ant!=null)
		{
			if(ant.darDato().darHod()==hora)
			{
			ccola.enqueue(ant.darDato());
			cont ++;

			}
		ant=ant.darSiguiente();
		}
		
		return ccola;
	}

	




}
