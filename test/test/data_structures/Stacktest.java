package test.data_structures;

import model.data_structures.Stack;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class Stacktest {

	private Stack <String> stack;
	
	private String uno="uno";
	
	private String dos = "dos";
	
	private String tres = "tres";
	
	private String cua = "cuatro";
	
	private String cin = "cinco";
	
	@Before
	public void SetUp1()
	{
		stack= new Stack<String>();
		stack.push(uno);
		stack.push(dos);
		stack.push(tres);
		stack.push(cua);
		stack.push(cin);
	}
	
	@Before
	public void setUpVacio()
	{
		stack= new Stack<String>();
	}
	
	@Test
	public void tama�o()
	{
		SetUp1();
		assertEquals("El tama�o es 5",5, stack.darTama�o() );
		setUpVacio();
		assertEquals("El tama�o es 0",0, stack.darTama�o() );
		
	}

	@Test
	public void pop()
	{
		SetUp1();
		stack.pop();
		assertEquals("El tama�o es 4",4, stack.darTama�o() );
		
	}
	
	@Test
	public void esVacio()
	{
		SetUp1();
		assertEquals("No est� vacio", false, stack.esVacio());
		setUpVacio();
		assertEquals("Est� vacio", true, stack.esVacio());
	}
	
	@Test
	public void primero()
	{
		SetUp1();
		assertEquals("El primero es cin", cin, stack.top().darDato());
		stack.pop();
		assertEquals("El primero ahora es 4",cua, stack.top().darDato() );
	}
	
	
	@Test
	public void push()
	{
		SetUp1();
		stack.push("seis");
		assertEquals("El primero es seis", "seis", stack.top().darDato());
		assertEquals("Tama�o es 6",6, stack.darTama�o() );
	}
}
